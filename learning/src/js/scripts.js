'use strict';

var items = document.getElementById('portfolio').getElementsByTagName('li'),
    doc = document.documentElement,
    itemImg = document.getElementsByClassName('showcase-img')[0],
    showcaseContent = document.getElementsByClassName('showcase-content')[0],
    closeBtn = document.getElementById('showcase-close'),
    nextBtn = document.getElementById('showcase-next'),
    prevBtn = document.getElementById('showcase-prev'),
    itemIndex;

var trim = function(str) {
  return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g,'');
};

var hasClass = function(el, cn) {
  return(' ' + el.className + ' ').indexOf(' ' + cn + ' ') !== -1;
};

var addClass = function(el, cn) {
  if (!hasClass(el, cn)) {
    el.className = (el.className === '') ? cn : el.className + ' ' + cn;
  }
};

var removeClass = function(el, cn) {
  el.className = trim((' ' + el.className + ' ').replace(' ' + cn + ' ', ' '));
};

var hideEl = function(el){
  if(el <= 0){
    addClass(prevBtn, 'hide');
  } else {
    removeClass(prevBtn, 'hide');
  }

  if(el >= items.length-1){
    addClass(nextBtn, 'hide');
  } else {
    removeClass(nextBtn, 'hide');
  }
};

var changeItem = function(e, isNext){

  var i = isNext ? itemIndex + 1 : itemIndex - 1;

  if(items[i].getElementsByTagName('a')[0].dataset.imglnk && items[i].getElementsByClassName('overlay-text')[0]){
    itemImg.setAttribute('src',items[i].getElementsByTagName('a')[0].dataset.imglnk);

    var cloneItem = items[i].getElementsByClassName('overlay-text')[0].cloneNode(true);
    showcaseContent.replaceChild(cloneItem, showcaseContent.getElementsByClassName('overlay-text')[0]);
  }

  itemIndex = i;
  hideEl(itemIndex);
  e.preventDefault();
};

_.each(items, function(item, index){

  item.getElementsByTagName('a')[0].addEventListener('click', function(e){
    addClass(doc,'showcase-active');
    window.scrollTo(0, 0);

    if(item.getElementsByTagName('a')[0].dataset.imglnk && item.getElementsByClassName('overlay-text')[0]) {
      document.getElementsByClassName('showcase-img')[0].setAttribute('src',item.getElementsByTagName('a')[0].dataset.imglnk);

      var cloneItem = item.getElementsByClassName('overlay-text')[0].cloneNode(true);
      showcaseContent.replaceChild(cloneItem, showcaseContent.getElementsByClassName('overlay-text')[0]);
    }

    itemIndex = index;
    hideEl(index);
    e.preventDefault();

  });

});

nextBtn.addEventListener('click', function(e){ changeItem(e, true); });
prevBtn.addEventListener('click', function(e){ changeItem(e, false); });
closeBtn.addEventListener('click', function(){ removeClass(doc, 'showcase-active'); });
