'use strict';

module.exports = function(grunt) {

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt);

    var devPath = 'src',
        buildPath = 'dist',
        tempPath = '.tmp',
        imageminJpegoptim = require('imagemin-jpegoptim'),
        imageminPngquant = require('imagemin-pngquant');

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        config: {
            app: devPath,
            dist: buildPath,
            temp: grunt.option('temp') ? grunt.option('temp') : tempPath
        },

        //compass
        compass: {
            dev: {
                options: {
                    debugInfo: false,
                    config: 'config.rb',
                    outputStyle: 'nested',
                    environment: 'development',
                    sourcemap: true
                }
            },
            dist: {
                options: {
                    cssDir: '<%= config.dist %>/assets/css',
                    outputStyle: 'compressed',
                    environment: 'production'
                }
            }
        },

        //postcss
        postcss: {
            options: {
                map: false,
            },
            dev: {
                options: {
                    processors: [
                        require('pixrem')(),
                        require('autoprefixer')({
                            browsers: 'last 1 version'
                        })
                    ]
                },
                src: '<%= config.temp %>/assets/css/{,*/}*.css'
            },
            dist: {
                options: {
                    processors: [
                        require('pixrem')(),
                        require('autoprefixer')({
                            browsers: 'last 1 version'
                        }),
                        require('cssnano')()
                    ]
                },
                src: '<%= config.dist %>/assets/css/{,*/}*.css'
            }
        },

        //htmlhint
        htmlhint: {
            options: {
                htmlhintrc: '.htmlhintrc'
            },
            dev: {
                src: '<%= config.temp %>/**/*.html'
            },
            dist: {
                src: '<%= config.dist %>/**/*.html'
            }
        },

        //jshint
        jshint: {
            files: ['Gruntfile.js', '<%= config.app %>/js/**/*.js', '!<%= config.app %>/js/vendors/**'],
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            }
        },

        //imagemin
        imagemin: {
            options: {
                svgoPlugins: [{ removeViewBox: false }],
                use: [
                    imageminPngquant({quality: '65-80', speed: 4}),
                    imageminJpegoptim({progressive: true, max: 70})
                ]
            },
            dev: {
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>',
                    src: ['img/**/*.{png,jpg,svg}'],
                    dest: '<%= config.temp %>/assets/'
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.app %>',
                    src: ['img/**/*.{png,jpg,svg}'],
                    dest: '<%= config.dist %>/assets/'
                }]
            }
        },

        //browser-sync
        browserSync: {
            bsFiles: {
                src : [
                    '<%= config.temp %>/assets/css/{,*/}*.css',
                    '<%= config.temp %>/**/*.html',
                    '<%= config.temp %>/assets/js/**/*.js'
                ]
            },
            options: {
                browser: 'Chrome',
                watchTask: true,
                server: {
                    baseDir: ['<%= config.temp %>'],
                    index: 'index.html'
                }
            }
        },

        //copy
        copy: {
            dev: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.temp %>/assets/',
                    src: [
                        'js/**/*',
                        'font/{,*/}*.*'
                    ]
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.dist %>/assets/',
                    src: [
                        'img/*.{ico,txt}',
                        'js/**/*',
                        'font/{,*/}*.*'
                    ]
                }
              ]
            },
            pagesTmp: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>/pages/',
                    dest: '<%= config.temp %>/',
                    src: '**'
                }]
            },
            pagesDist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>/pages/',
                    dest: '<%= config.dist %>/',
                    src: '**'
                }]
            },
            vendorsTmp: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>/scss/',
                    dest: '<%= config.temp %>/assets/css/',
                    src: 'vendors/{,*/}*.*'
                }]
            },
            vendorsDist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.app %>/scss/',
                    dest: '<%= config.dist %>/assets/css/',
                    src: 'vendors/{,*/}*.*'
                }]
            },
        },

        //clean
        clean: {
            all: {
                options: {
                    force: true
                },
                src: [
                    '<%= config.dist %>',
                    '<%= config.temp %>',
                    '.sass-cache'
                ]
            },
            dev: [
                '<%= config.temp %>',
                '.sass-cache'
            ]
        },

        //watch
        watch: {
            html: {
                files: ['<%= config.app %>/pages/**/*.*'],
                tasks: ['newer:copy:pagesTmp', 'htmlhint:dev']
            },
            compass: {
                files: ['<%= config.app %>/scss/**/*.scss'],
                tasks: ['compass:dev', 'postcss:dev']
            },
            js: {
                files: ['<%= jshint.files %>'],
                tasks: ['jshint']
            }
        }

    });


    grunt.registerTask('default', ['clean:dev', 'compass:dev', 'postcss:dev', 'jshint', 'htmlhint:dev', 'imagemin:dev', 'copy:dev', 'copy:pagesTmp', 'copy:vendorsTmp', 'browserSync', 'watch']);
    grunt.registerTask('build', ['clean:all', 'compass:dist', 'postcss:dist', 'jshint', 'htmlhint:dist', 'imagemin:dist', 'copy:dist', 'copy:pagesDist', 'copy:vendorsDist', ]);
    grunt.registerTask('cleanUp', ['clean:all']);

};
